from psychopy import gui, visual, sound, core, event, data, logging, monitors
import ld, csv, copy, math

'''
====================
Recommended Citation
====================
Dobres, J., Chahine, N., & Reimer, B. (2017). Effects of ambient illumination,
contrast polarity, and letter size on text legibility under glance-like reading.
Applied Ergonomics, 60(C), 68–73.

============================
Released Under a BSD License
============================
Copyright 2022 Massachusetts Institute of Technology

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'''

settings = ld.importSettings('config/settings.csv')
win = list();
pixels_per_cm = list()
sample_indices = [0, 0];
for w in range(settings['nScreens']):
    win.append(visual.Window([1000, 1000], allowGUI=True, winType='pyglet', units ='pix', screen=w, fullscr=True, waitBlanking=True));
    pixels_per_cm.append(float(settings['screen-%i-horiz-px' % w]) / float(settings['screen-%i-horiz-cm' % w]));

mice = list()
for w in win:
    mice.append(event.Mouse(win=w));

selectionRight = visual.Rect(win[0], width=10, height=10, fillColor='#00ff00', lineColor='#000000', pos=[win[0].size[0] / 2 - 10, 0])
selectionLeft = copy.copy(selectionRight)
selectionLeft.pos = [-win[0].size[0] / 2 + 10, 0]

conditionData = csv.DictReader(open('config/conditions.csv', 'rU'));
conditions = []
for (i, row) in enumerate(conditionData):
    screen_i = int(row['screen'])
    row.update({
        'background' : visual.Rect(win[screen_i],
        units='pix',
        pos=[0, (round(sample_indices[screen_i] * -2 * pixels_per_cm[screen_i])) + 100],
        fillColor=row['background'],
        lineColor='#ff0000',
        height=float(row['size_mm']) * 0.1 * pixels_per_cm[screen_i] + 0, width=win[screen_i].size[0] + 0,
        interpolate=True,
        lineWidth=0
        ),
        'text' : visual.TextStim(win[screen_i],
            bold=row['bold'] == 'TRUE',
            italic=row['italic'] == row['italic'] == 'TRUE',
            wrapWidth=10000,
            text=settings['testText'] + ' ' + row['section'] + ' ' + row['font'] + ' ' + row['size'],
            color=row['foreground'],
            font=row['font'],
            height=float(row['size']),
            pos=[0, sample_indices[screen_i] * (round(-pixels_per_cm[screen_i] * 2)) + 100],
        )
    })
    conditions.append(row)
    sample_indices[screen_i] += 1;
    instructions = visual.TextStim(
        win[0],
        text='Each box shows a text sample, the condition name, condition font, and current size in pixels.\n\nClick a strip to adjust the font inside it.\n  - left/right arrow = adjust size\n  - up/down arrow = adjust position\n  - escape = quit',
        pos=[0, win[0].size[1] / 2 - 100], # win[0].size[1] / 2
        anchorVert = 'top',
        color='#ffffff'
    )

selectedBox = []
selectedText = []
k = []
while k == [] or k[0] != 'escape':
    for (i, c) in enumerate(conditions):
        screen_i = int(c['screen']);
        for mouse in mice:

            if mouse.isPressedIn(c['background']) and c['background'] != selectedBox:
                event.clearEvents();
                c['background'].lineColor = c['background'].fillColor
                selectedBox = c['background']
                selectedText = c['text']
                selectedIndex = i
                selectionLeft.win = win[screen_i];
                selectionRight.win = win[screen_i];
                selectionLeft.pos = [-win[screen_i].size[0] / 2 + 10, selectedText.pos[1]]
                selectionRight.pos = [win[screen_i].size[0] / 2 - 10, selectedText.pos[1]]
            elif c['background'] != selectedBox:
                c['background'].lineColor = c['background'].fillColor

            c['background'].draw()
            c['text'].draw()

        instructions.draw()
        if selectedBox != []:
            selectionLeft.draw()
            selectionRight.draw()

    k = event.getKeys()

    if selectedBox != [] and len(k) > 0:
        if k[0] == 'right':
            selectedText.height = selectedText.height + 1
            selectedText.text = settings['testText'] + ' ' + conditions[selectedIndex]['section'] + ' ' + conditions[selectedIndex]['font'] + ' ' + str(selectedText.height)
        elif k[0] == 'left':
            selectedText.height = selectedText.height - 1
            selectedText.text = settings['testText'] + ' ' + conditions[selectedIndex]['section'] + ' ' + conditions[selectedIndex]['font'] + ' ' + str(selectedText.height)
        elif k[0] == 'up':
            selectedText.pos = [0, selectedText.pos[1] + 1]
        elif k[0] == 'down':
            selectedText.pos = [0, selectedText.pos[1] - 1]
    for w in range(len(win)):
        win[w].flip()
        win[w].getMovieFrame(buffer='front')
        win[w].saveMovieFrames('monitor %i samples.png' % w)

core.quit()
