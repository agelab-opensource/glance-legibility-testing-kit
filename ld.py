from psychopy import visual, sound, core, event, data, gui, logging
import copy, random, math, glob, PIL, csv, numpy as np

'''
====================
Recommended Citation
====================
Dobres, J., Chahine, N., & Reimer, B. (2017). Effects of ambient illumination,
contrast polarity, and letter size on text legibility under glance-like reading.
Applied Ergonomics, 60(C), 68–73.

============================
Released Under a BSD License
============================
Copyright 2022 Massachusetts Institute of Technology

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'''

class TextArray:
    def __init__(self, win, textItems, *args, **kwargs):

        # set most properties
        self.textObjects = []
        self.win = win
        self.items = textItems
        self.color = '#ffffff' if 'color' not in kwargs else kwargs['color']
        self.height = 20 if 'height' not in kwargs else kwargs['height']
        self.pos = (0, 0) if 'pos' not in kwargs else kwargs['pos']
        self.font = 'Courier' if 'font' not in kwargs else kwargs['font']
        self.bold = False if 'bold' not in kwargs else kwargs['bold']
        self.italic = False if 'italic' not in kwargs else kwargs['italic']
        self.alignText = 'left' if 'alignText' not in kwargs else kwargs['alignText']
        self.anchorHoriz = 'left'
        self.anchorVert = 'bottom'
        self.nRows = self.minSize() if 'nRows' not in kwargs else kwargs['nRows']
        self.leading = 0 if 'leading' not in kwargs else kwargs['leading']
        self.columnMargin = 20 if 'columnMargin' not in kwargs else kwargs['columnMargin']
        self.nCols = int(math.ceil(float(len(self.items)) / float(self.nRows)))

        # this stimulus supports setting text attributes per-item (but not text sizes, for now).
        # ideally each attribute should be either a single value for all items, or an array of the same length as the word array
        # if the array is too short, items are replicated until the length is matched
        self.color = self.rep(self.color)
        self.font = self.rep(self.font)
        self.bold = self.rep(self.bold)
        self.italic = self.rep(self.italic)

        # initial creation of individual text objects
        for i in range(len(self.items)):
            self.textObjects.append(visual.TextStim(
                self.win,
                text=self.items[i],
                color=self.color[i],
                height=self.height,
                font=self.font[i],
                bold=self.bold[i],
                italic=self.italic[i],
                alignText=self.alignText,
                anchorHoriz=self.anchorHoriz,
                anchorVert = self.anchorVert
            ))

        # determine maximal width for each column
        self.colWidths = [0] * self.nCols
        for i, item in enumerate(self.textObjects):
            currentCol = int(math.floor(i / self.nRows))
            self.colWidths[currentCol] = item.boundingBox[0] if item.boundingBox[0] > self.colWidths[currentCol] else \
            self.colWidths[currentCol]

        # set relative x and y coordinates for each item in array
        x = 0
        for i, item in enumerate(self.textObjects):
            currentCol = int(math.floor(i / self.nRows))
            if i != 0 and i % self.nRows == 0:
                x = x + self.colWidths[currentCol - 1]

            y = (self.nRows - 1 - i) % self.nRows
            y += y * (item.boundingBox[1] + self.leading)

            item.pos = (x + self.columnMargin * currentCol, y)

        # create a "boundingBox" property that represents the width and height of the array
        self.width = self.textObjects[-1].pos[0] + self.textObjects[-1].boundingBox[0]
        self.height =  self.textObjects[self.nRows - 1].pos[1] - self.textObjects[self.nRows - 1].boundingBox[1] - self.textObjects[0].pos[1]
        self.boundingBox = (self.width, self.height)

        # align individual coordinates to current array origin point
        self.pos = (-self.width / 2, self.height / 2)
        self.alignToPos(self.pos)

    def rep(self, s):
        theArray = s if isinstance(s, list) else [s]
        theArray *= len(self.items)
        theArray = theArray[:len(self.items)]
        return (theArray)

    def minSize(self):
        return math.ceil(math.sqrt(len(self.items)))

    def alignToPos(self, newPos):
        # align individual coordinates to current array origin point
        self.pos = newPos

        for i, item in enumerate(self.textObjects):
            item.pos[0] += newPos[0]
            item.pos[1] += newPos[1]

    def draw(self):
        #o_box = (self.pos, (self.pos[0] + self.boundingBox[0], self.pos[1]), (self.pos[0] + self.boundingBox[0], self.pos[1] + self.boundingBox[1]), (self.pos[0], self.pos[1] + self.boundingBox[1]))
        #this_shape = visual.ShapeStim(self.win, vertices = o_box, lineColor = None, fillColor = '#ffcccc', interpolate = False)
        #this_shape.draw()

        for o in self.textObjects:
            #o_box = (o.pos, (o.pos[0] + o.boundingBox[0], o.pos[1]), (o.pos[0] + o.boundingBox[0], o.pos[1] + o.boundingBox[1]), (o.pos[0], o.pos[1] + o.boundingBox[1]))
            #this_shape = visual.ShapeStim(self.win, vertices = o_box, lineColor = None, fillColor = '#ffcccc', interpolate = False)
            #this_shape.draw()
            o.draw()


def importSettings(file):
    import csv, pyglet
    settingsFile = csv.reader(open(file, 'rU'))
    settings = {}
    for row in settingsFile:
        settings[row[0]] = row[1]

    # Checks to ensure that:
    #   1. Number of monitors specified in settings file ('nScreens') matches the number actually connected.
    #   2. That each monitor has a specified horizontal resolution and size in settings ('monitor-#-horiz-px' and 'monitor-#-horiz-cm', starting at 0).
    #   3. That the specified resolution for each monitor matches its actual resolution.
    allScrs = pyglet.canvas.get_display().get_screens()
    numScrs = len(allScrs)
    settings['nScreens'] = 1 if 'nScreens' not in settings else int(settings['nScreens'])
    for i in range(settings['nScreens']):
        screen_i_px = 'screen-%i-horiz-px' % i
        screen_i_cm = 'screen-%i-horiz-cm' % i
        if screen_i_px not in settings or screen_i_cm not in settings:
            raise Exception(
                'Experiment settings specify %i monitors but settings for monitor %i were not found (remember to specify \'screen-%i-horiz-px\' and \'screen-%i-horiz-cm\'.' % (
                settings['nScreens'], i + 1, i, i))
        if numScrs < settings['nScreens']:
            raise Exception('Experiment settings specify %i monitor(s) but found %i.' % (settings['nScreens'], numScrs))
        if allScrs[i].width != int(settings[screen_i_px]):
            raise Exception(
                'Monitor #%i has resolution of %i pixels in settings file, but is actually set to %i pixels. Please fix!' % (
                i, int(settings[screen_i_px]), allScrs[i].width))

    # specify a size for the fixation rectangle
    settings['rectWidth'] = 200 if 'rectWidth' not in settings else float(settings['rectWidth'])
    settings['rectHeight'] = 100 if 'rectHeight' not in settings else float(settings['rectHeight'])
    settings['rectX'] = 0 if 'rectX' not in settings else float(settings['rectX'])
    settings['rectY'] = 0 if 'rectY' not in settings else float(settings['rectY'])
    settings['promptSize'] = 30.0 if 'promptSize' not in settings else float(settings['promptSize'])

    settings['breakLengthWithinCondition'] = 30 if 'breakLengthWithinCondition' not in settings else int(settings['breakLengthWithinCondition'])
    settings['breakLengthBetweenCondition'] = 30 if 'breakLengthBetweenCondition' not in settings else int(settings['breakLengthBetweenCondition'])
    settings['breakLengthBetweenGroup'] = 30 if 'breakLengthBetweenGroup' not in settings else int(settings['breakLengthBetweenGroup'])

    settings['breakOptionalWithinCondition'] = True if 'breakOptionalWithinCondition' not in settings else settings['breakOptionalWithinCondition'] == 'TRUE'
    settings['breakOptionalBetweenCondition'] = True if 'breakOptionalBetweenCondition' not in settings else settings['breakOptionalBetweenCondition'] == 'TRUE'
    settings['breakOptionalBetweenGroup'] = True if 'breakOptionalBetweenGroup' not in settings else settings['breakOptionalBetweenGroup'] == 'TRUE'

    return settings


def importMessages(file):
    import csv
    messagesFile = csv.reader(open(file, 'rU', encoding = 'utf-8'))
    messages = {}
    for row in messagesFile:
        row[1] = row[1].replace('\\n', '\n')
        messages[row[0]] = row[1]

    return messages


def startupGUI(messages, settings):
    # When OK is clicked, a basic correctness check is performed. If all fields are filled out, experiment proceeds.
    # If not, dialog remains (has to be recreated in loop because PsychoPy changes dialog attributes "in place")
    # If Cancel is clicked, experiment quits.

    info = {messages['guiID']: '',
            messages['guiGender']: '',
            messages['guiAge']: '',
            messages['guiSession']: 1}
    while info[messages['guiID']] == '' or len(info[messages['guiGender']]) == 2 or info[messages['guiAge']] == '':
        infoLoop = info
        infoDlg = gui.DlgFromDict(dictionary=infoLoop, title='')
        if not infoDlg.OK:
            core.quit()

    return info


def checkForPreviousData(messages, settings, subjectInfo):
    files = glob.glob('Data/%s participant %s session %s*' % (
    settings['name'], subjectInfo[messages['guiID']], subjectInfo[messages['guiSession']]))
    if len(files) > 0:
        files = files[len(files) - 1]  # get most recent filename matching this subject and session
        info = {messages['guiDataDecision']: [messages['guiDataRestart'], messages['guiDataResume']]}
        dlg = gui.DlgFromDict(dictionary=info, title='Previous data found: %s' % files)
        if dlg.OK:
            isResuming = info[messages['guiDataDecision']] == messages['guiDataResume']
            return {'isResuming': isResuming, 'file': files}
        else:
            core.quit()
    else:
        return {'isResuming': False}


def loadPreviousData(dataFile, messages, settings):
    import csv

    # load previous session's trial data and set the trial counters to the last trial performed
    trialData = csv.DictReader(open(dataFile, 'rU'))
    trials = []
    t = -1
    blockCounter = 0
    blockStart = -1
    for (i, row) in enumerate(trialData):
        row['nDistractors'] = 0 if 'nDistractors' not in row else int(row['nDistractors'])
        row['nRows'] = 0 if 'nRows' not in row else int(row['nRows'])
        row['targetRow'] = 1 if 'targetRow' not in row else int(row['targetRow'])
        row['leadingPercent'] = 0 if 'leadingPercent' not in row else float(row['leadingPercent'])
        row['columnMargin'] = 0 if 'columnMargin' not in row else int(row['columnMargin'])
        row['size'] = float(row['size'])
        row['isWord'] = row['isWord'] == 'True'
        row['giveFeedback'] = row['giveFeedback'] == 'True'
        row['completed'] = row['completed'] == 'True'
        trials.append(row)
        t = i if t == -1 and not row['completed'] else t
        blockCounter = 0 if (i == 0 or row['section'] != prevTrial['section']) else blockCounter + 1

        if t > -1 and blockStart == -1:
            blockStart = blockCounter - 0

        prevTrial = row
    t = 0 if t < 0 else t

    # re-establish the staircase by running through all completed trials
    bc = 0
    for i in range(t):
        bc = 0 if t == 0 or trials[t]['section'] != trials[t - 1]['section'] else bc

        if bc == 0:
            stair = setStair(float(settings['monitorHz']))

        if bc > 8 and trials[t - 1]['correct'] != 'NA':
            correct = trials[t - 1]['correct'] == 'True'
            stair.addResponse(correct)
            stair.calculateNextIntensity()
            stair.next()
        bc = bc + 1

    # trials, t, blockStart, and stair
    return {'trials': trials, 't': t, 'blockCounter': blockCounter, 'stair': stair}


def introScreen(win, message, settings):
    logo = visual.ImageStim(win, image='images/logo.png', size=[364, 414], interpolate=True, pos=[0, 100])
    introText = visual.TextStim(win, text=message, font=settings['promptFont'],
                                pos=[0, logo.pos[1] - logo.size[1] / 2 - 50], height=30, color='#ffffff',
                                anchorVert = 'top')

    logo.draw()
    introText.draw()
    win.flip()
    event.waitKeys()


def instructionScreen(win, messages, settings, section):
    # This instruction screen uses a single image to display instructions.

    font = settings['promptFont']
    fontSize = settings['promptSize']
    imInstruct = visual.ImageStim(win, image='images/instructions.png', pos=[0, 0])
    imInstruct = visual.ImageStim(win, image='images/instructions.png', pos=[0, 0],
                                  size=(imInstruct.size[0] * 1.0, imInstruct.size[1] * 1.0))

    imInstruct.draw()

    win.flip()
    k = event.waitKeys(keyList=['1', 'num_1'])

    win.flip()
    core.wait(1)

    if section == 'practice':
        practiceText = visual.TextStim(win, text=messages['instructPractice'], color='#ffffff', font=font,
                                       height=fontSize * 1.33)
        practiceText.draw()
        win.flip()
        k = event.waitKeys(keyList=['1', 'num_1'])


def practiceAgain(win, messages, settings, nextDistance):
    win.flip()
    core.wait(1)

    txtPracticeAgain = visual.TextStim(win, text=messages['instructPracticeAgain'], color='#ffffff',
                                       font=settings['promptFont'], height=int(settings['promptSize']))
    txtPracticeAgain.draw()
    win.flip()
    k = event.waitKeys(keyList=['1', '3', 'num_1', 'num_3'])

    if k[0] == '3' or k[0] == 'num_3':
        win.flip()
        core.wait(1)
        txtFinal = visual.TextStim(win, text=messages['instructFinal'], color='#ffffff',
                                   font=settings['promptFont'], height=int(settings['promptSize']), wrapWidth=1000)

        txtFinal.draw()
        win.flip()
        event.waitKeys(keyList=['1', 'num_1'])

        win.flip()
        core.wait(1)

    return True if k[0] == '1' or k[0] == 'num_1' else False


def endScreen(win, messages, settings, trials):
    pc = 0.0
    for t in trials:
        pc = pc + 1 if 'correct' in t and (t['correct'] or t['correct'] == 'True') else pc
    pc = pc / float(len(trials)) * 100

    logo = visual.ImageStim(win, image='images/logo.png', size=[364, 414], interpolate=True, pos=[0, 100])
    endText = visual.TextStim(win, text=messages['end'] % pc, font=settings['promptFont'],
                              pos=[0, logo.pos[1] - logo.size[1] / 2 - 40], height=float(settings['promptSize']),
                              color='#ffffff', anchorVert = 'top')

    logo.draw()
    endText.draw()
    win.flip()
    event.waitKeys()


def loadStimuli(wordFile, nonWordFile, distractorFile=None):
    import csv

    # load words
    wordsIn = csv.reader(open(wordFile, 'rU'))
    words = []
    for row in wordsIn:
        words.append((row[0]))

    # load nonwords
    nonWordsIn = csv.reader(open(nonWordFile, 'r', encoding = 'utf-8'))
    nonWords = []
    for row in nonWordsIn:
        nonWords.append((row[0]))

    # load distractors
    if distractorFile is not None:
        disIn = csv.reader(open(distractorFile, 'rU'))
        dis = []
        for row in disIn:
            dis.append((row[0]))

    return {'words': words, 'nonwords': nonWords, 'distractors': None if distractorFile == None else dis}


def makeConditions(conditionFile, wordFile, nonWordFile, distractorFile, monitorHz, subjectInfo):

    # import word lists and shuffle into random order
    random.seed()
    stimulusText = loadStimuli(wordFile, nonWordFile, distractorFile)
    random.shuffle(stimulusText['words'])
    random.shuffle(stimulusText['nonwords'])
    if distractorFile is not None:
        random.shuffle(stimulusText['distractors'])

    # store a copy of the distractor words in case we need to recycle them
    # 'distractors' will gradually shrink until we need to reset with 'distractorMaster'
    distractorMaster = stimulusText['distractors']
    distractors = copy.copy(distractorMaster)

    # load all conditions. assume first condition always corresponds to the "practice round"
    conditionData = csv.DictReader(open(conditionFile, 'rU'))

    # pre-allocation for the condition-shuffling nightmare
    groups = []
    groups_final = []
    cycled_groups = []
    grouped_data = []
    conditions = []
    conditions_final = []
    trials = []

    # The code below does the following:
    #   1. Check to ensure that each condition has:
    #       - 'condition_order'. Default: 'fixed' if section == 'practice', otherwise 'random'
    #       - 'group'. Default: 1
    #       - 'group_order'. Default: 'random'
    #   2. Unique groups are counted.
    #   3. Any group designated as 'fixed' retains its chronological order as specified in conditions.csv
    #   4. Any group designated 'random' is shuffled among the available random group positions
    #   5. Within each group, for each condition (single row in conditions.csv), perform steps 3 and 4.

    # Steps 1 & 2
    for i, row in enumerate(conditionData):
        if 'condition_order' not in row:
            row['condition_order'] = 'fixed' if row['section'] == 'practice' else 'random'

        row['group'] = 1 if 'group' not in row else row['group']
        row['group_order'] = 'random' if 'group_order' not in row else row['group_order']

        if row['group'] not in cycled_groups:
            groups.append({'index': len(cycled_groups), 'id': row['group'], 'order': row['group_order']})
            cycled_groups.append(row['group'])

        conditions.append(row)

    # Steps 3 & 4
    groups_random = [g['index'] for g in groups if g['order'] == 'random']
    random.shuffle(groups_random)
    random_available = copy.copy(groups_random)
    groups_fixed = [g['index'] for g in groups if g['order'] == 'fixed']

    for i, g in enumerate(groups):
        if i in groups_fixed:
            groups_final.append(g)
        elif i in groups_random:
            groups_final.append(groups[random_available.pop(0)])

    # Step 5
    for i, g in enumerate(groups_final):
        this_group = [a for a in conditions if a['group'] == g['id']]

        for ii, gg in enumerate(this_group):
            gg['index'] = ii

        grouped_data.append(this_group)

    for g in grouped_data:

        # find all randomizable conditions
        # shuffle the order, and make two copies (one for looping, one for actually selecting conditions)

        indices_random = [a['index'] for a in g if a['condition_order'] == 'random']
        random.shuffle(indices_random)
        random_available = copy.copy(indices_random)

        # indices of fixed conditions
        indices_fixed = [a['index'] for a in g if a['condition_order'] == 'fixed']

        # loop through the specified conditions
        # if the current index matches the index of a fixed condition, insert it
        # if the current index matches a random condition, get the first one available from random_available
        for i in range(len(g)):
            if i in indices_fixed:
                conditions_final.append(g[i])
            elif i in indices_random:
                conditions_final.append(g[random_available.pop(0)])

    # create a dictionary in trials[] for each trial of the experiment.
    # Holds stimulus attributes, the word to use, whether to give feedback, etc.
    grandCount = 1
    for c, row in enumerate(conditions_final):
        nWords = int(row['nWords'])
        nNonWords = int(row['nNonWords'])
        isWord = [True] * nWords + [False] * nNonWords
        random.shuffle(isWord)

        # creates a list of presentation trials for the initial staircase descent
        # note that the staircase won't activate until trial 9, and its settings ensure 3 trials at 200ms, thus continuing the descent
        descendingTrials = [int(round(i * monitorHz)) for i in [0.8, 0.8, 0.8, 0.6, 0.6, 0.6, 0.4, 0.4, 0.4]]

        for t in range(nWords + nNonWords):
            thisTrial = dict(row)

            if thisTrial['textPosition'] not in ('center', 'random'):
                raise Exception('textPosition property for conditions must be either \'center\' or \'random\'.')

            del thisTrial['nWords']
            del thisTrial['nNonWords']
            thisTrial['size'] = int(thisTrial['size'])
            thisTrial['giveFeedback'] = thisTrial['giveFeedback'] == 'TRUE'
            thisTrial['uppercase'] = thisTrial['uppercase'] == 'TRUE'
            thisTrial['isWord'] = isWord[t]
            thisTrial['completed'] = False

            thisTrial['background_blur'] = 0 if 'background_blur' not in thisTrial else float(thisTrial['background_blur'])
            thisTrial['duration'] = 'stair' if 'duration' not in thisTrial else thisTrial['duration']
            thisTrial['textPosition'] = 'center' if 'textPosition' not in thisTrial else thisTrial['textPosition']
            thisTrial['screen'] = 0 if 'screen' not in thisTrial else int(thisTrial['screen'])
            thisTrial['distance'] = 'NA' if 'distance' not in thisTrial else thisTrial['distance']
            thisTrial['nRows'] = -1 if 'nRows' not in thisTrial else int(row['nRows'])
            thisTrial['nCols'] = -1 if 'nCols' not in thisTrial else int(row['nCols'])
            thisTrial['nDistractors'] = thisTrial['nRows'] * thisTrial['nCols'] - 1
            thisTrial['leadingPercent'] = 0 if 'leadingPercent' not in thisTrial else float(row['leadingPercent'])
            thisTrial['columnMargin'] = 0 if 'columnMargin' not in thisTrial else int(row['columnMargin'])
            thisTrial['trial'] = t + 1
            thisTrial['grand.trial'] = grandCount
            grandCount += 1

            # insert stimulus text for each trial
            if isWord[t]:
                thisTrial['text'] = stimulusText['words'].pop()
            else:
                thisTrial['text'] = stimulusText['nonwords'].pop()

            # pick a random row between 2 and 4 for the target location
            if (int(row['nRows']) > 1):
                thisTrial['targetRow'] = random.randint(2, int(row['nRows']) - 1)
            else:
                thisTrial['targetRow'] = 1

            # pick out a set of distractor words and insert the target at the appropriate index

            # if there are not enough words left in the distractor array, shuffle 'distractorMaster' and recycle it
            if len(distractors) < thisTrial['nDistractors']:
                random.shuffle(distractorMaster)
                distractors = copy.copy(distractorMaster)

            thisTrial['wordList'] = distractors[:thisTrial['nDistractors']]
            del distractors[:thisTrial['nDistractors']]
            targetPos = int(row['nRows']) - 1 + thisTrial['targetRow']
            thisTrial['wordList'].insert(targetPos, thisTrial['text'])
            thisTrial['wordList'] = ' '.join(thisTrial['wordList'])

            # if this is part of the initial staircase descent, insert a predefined presentation time
            # if this is practice trial, presentation time is always equal to 1.0sec

            if thisTrial['duration'] == 'stair':
                if len(descendingTrials) > 0:
                    thisTrial['presentationFrames'] = descendingTrials.pop(0)
                else:
                    thisTrial['presentationFrames'] = 'NA'
            elif thisTrial['duration'] == 'calibrated':
                thisTrial['presentationFrames'] = 'calibrated'
            elif thisTrial['duration'] == 'rt':
                thisTrial['presentationFrames'] = 'open response'
            else:
                thisTrial['presentationFrames'] = round(float(thisTrial['duration']))

            thisTrial.update(subjectInfo)

            trials.append(thisTrial)

    return trials


def makeBreaks(trials, blockLength):
    firstTrial = 0
    numTrials = 0
    for i, t in enumerate(trials):
        numTrials = i if t['section'] != 'practice' else numTrials
        firstTrial = i if t['section'] == 'practice' else firstTrial
    numTrials = numTrials + 1
    firstTrial = firstTrial + blockLength
    return range(firstTrial, numTrials - blockLength, blockLength)


def breakScreen(win, introMsg, continueMsg, settings, breakLength, allowSkip, blocksDone, nBlocks):

    background = visual.Rect(win,
                             width=win.size[0],
                             height=win.size[1],
                             fillColor='#000000',
                             lineColor='#000000')

    font = settings['promptFont']
    fontSize = 30
    timerText = visual.TextStim(win, text='', color='#ffffff', height=fontSize * 3, font=font)
    blockText = visual.TextStim(win, text=introMsg % (blocksDone, nBlocks), color="#ffffff",
                                height=fontSize, font=font, pos=(0, timerText.pos[1] + fontSize * 4), wrapWidth=1000)
    continueText = visual.TextStim(win, continueMsg,
                                   color="#ffffff", height=fontSize, font=font,
                                   pos=(0, timerText.pos[1] - fontSize * 7), wrapWidth=1000)

    timer = core.CountdownTimer(breakLength)
    k = []
    while not (allowSkip and len(k) > 0) and timer.getTime() > 0:
        timerText.text = '%i' % (math.ceil(timer.getTime()))
        background.draw()
        blockText.draw()
        timerText.draw()

        if timer.getTime() < breakLength - 1:
            continueText.draw()

        win.flip()

        k = event.getKeys(['1', 'num_1'])


def pauseScreen(win, messages, settings, subjectInfo):
    pauseText = visual.TextStim(win, text=messages['pause'], font=settings['promptFont'],
                                height=float(settings['promptSize']), color='#ffffff')
    pauseText.draw()
    win.flip()
    k = event.waitKeys(keyList=['1', '3', 'num_1', 'num_3'])

    if (k[0] in ['3', 'num_3']):
        saveFile = 'Data/timings %s participant %s session %s.txt' % (
        settings['name'], subjectInfo[messages['guiID']], subjectInfo[messages['guiSession']])
        win.saveFrameIntervals(fileName = saveFile, clear = True)
        core.quit()


def setStair(monitorHz):
    # resets the staircase for new blocks
    # note that the trial limit for the stair ('nTrials' parameter) is arbitrarily large, as we will be resetting the staircases manually
    stair = data.StairHandler(int(monitorHz * 0.400),
                              nReversals=5,
                              stepSizes=[round(monitorHz * 0.05), round(monitorHz * 0.05), 2, 2, 1],
                              nTrials=1000, nUp=1, nDown=3, stepType='lin', minVal=1, maxVal=int(monitorHz))
    return stair


def makeMaskString(size=6, chars='=<>|^'):
    return ''.join(random.choice(chars) for x in range(size))

def make_all_backgrounds(win, conditionFile, settings):

    backgrounds = {}

    # load all conditions
    conditionData = csv.DictReader(open(conditionFile, 'rU'))

    for i, row in enumerate(conditionData):
        stimulus_i = {'window': win, 'foreground': row['foreground'], 'background': row['background']}
        settings_i = {'rectWidth': settings['rectWidth'], 'rectHeight': settings['rectHeight']}
        blur_i = float(row['background_blur'])
        im_i = make_background(stimulus_i, settings_i, blur = blur_i)
        backgrounds[row['section']] = im_i

    return backgrounds


def make_background(stimulus, settings, blur):
    from PIL import ImageFilter
    win = stimulus['window']

    rectWidth = settings['rectWidth']
    rectHeight = settings['rectHeight']

    # background
    background = visual.Rect(win,
                             width=win.size[0],
                             height=win.size[1],
                             fillColor='#000000',
                             lineColor='#000000')

    # fixation box elements
    fixationFrame = visual.Rect(win,
                                width=rectWidth,
                                height=rectHeight,
                                lineColor=stimulus['foreground'],
                                fillColor=stimulus['background'],
                                lineWidth=2,
                                interpolate=False)

    # create blurred image
    win.clearBuffer()
    background.draw()
    fixationFrame.draw()

    im = visual.BufferImageStim(win) # create PsychoPy image
    im_blurred = im.image.filter(ImageFilter.GaussianBlur(radius = blur)) # blur
    im.image = im_blurred # reinert into PsychoPy object

    win.clearBuffer()
    return im


def displayCrowdingStimulus(stimulus, settings, messages, checkStimulusTimings = False):
    win = stimulus['window']
    monitorHz = float(settings['monitorHz'])
    frameInterval = 1 / monitorHz

    cueFrames = monitorHz * 1.5
    fixationFrames = float(round(0.4 * monitorHz))
    mask1Frames = float(stimulus['maskInDuration']) * monitorHz
    stimulusFrames = float(stimulus['presentationFrames'])
    mask2Frames = float(stimulus['maskOutDuration']) * monitorHz
    totalFrames = cueFrames + fixationFrames + mask1Frames + stimulusFrames + mask2Frames

    if (stimulus['uppercase']):
        stimulus['text'] = stimulus['text'].upper()

    rectWidth = settings['rectWidth']
    rectHeight = settings['rectHeight']
    stimulus['nItems'] = len(stimulus['wordList'].split(' '))
    stimulus['leading'] = round(stimulus['leadingPercent'] * stimulus['size'])

    # background
    background_im = stimulus['background_image']

    # cue text
    cueText = visual.TextStim(win,
                              text=messages['cue'] % stimulus['targetRow'],
                              color=stimulus['foreground'],
                              font=settings['promptFont'],
                              height=settings['promptSize']
                              )

    # masks
    mask1Text = []
    mask2Text = []
    for i in range(stimulus['nItems']):
        mask1Text.append(makeMaskString(size=len(stimulus['text']) + 0))
        mask2Text.append(makeMaskString(size=len(stimulus['text']) + 0))

    mask1 = TextArray(win,
                      mask1Text,
                      font=stimulus['font'],
                      bold=stimulus['bold'] == 'TRUE',
                      italic=stimulus['italic'] == 'TRUE',
                      color=stimulus['foreground'],
                      height=stimulus['size'],
                      nRows=stimulus['nRows'],
                      leading=stimulus['leading'],
                      columnMargin=stimulus['columnMargin']
                      )

    mask2 = TextArray(win,
                      mask2Text,
                      font=stimulus['font'],
                      bold=stimulus['bold'] == 'TRUE',
                      italic=stimulus['italic'] == 'TRUE',
                      color=stimulus['foreground'],
                      height=stimulus['size'],
                      nRows=stimulus['nRows'],
                      leading=stimulus['leading'],
                      columnMargin=stimulus['columnMargin']
                      )

    # stimulus text
    stimulusText = TextArray(win,
                             stimulus['wordList'].split(' '),
                             font=stimulus['font'],
                             bold=stimulus['bold'] == 'TRUE',
                             italic=stimulus['italic'] == 'TRUE',
                             color=stimulus['foreground'],
                             height=stimulus['size'],
                             nRows=stimulus['nRows'],
                             leading=stimulus['leading'],
                             columnMargin=stimulus['columnMargin']
                             )

    if stimulus['textPosition'] == 'random':
        thePosition = (random.uniform(-rectWidth / 2 + 20, rectWidth / 2 - mask1.boundingBox[0] - 20),
                       random.uniform(-rectHeight / 2 + 20, rectHeight / 2 - mask2.boundingBox[1] - 20)
                       )
    elif stimulus['textPosition'] == 'center':
        thePosition = (-mask1.boundingBox[0] / 2, -mask1.boundingBox[1] / 2)

    #mask1.alignToPos(thePosition)
    #mask2.alignToPos(thePosition)
    #stimulusText.alignToPos(thePosition)

    # this loop counts frames and displays the stimulus
    # the commented code in this section is a rough way to verify stimulus timing
    win.recordFrameIntervals = True
    for frameN in range(0, int(totalFrames)):
        background_im.draw()
        if frameN < cueFrames:
            cueText.draw()
        elif frameN < cueFrames + fixationFrames:
            pass
        elif frameN < cueFrames + fixationFrames + mask1Frames:
            mask1.draw()
        elif frameN < cueFrames + fixationFrames + mask1Frames + stimulusFrames:
            stimulusText.draw()
            # if frameN == fixationFrames + mask1Frames:
            # onset = core.getTime()
        elif frameN < cueFrames + fixationFrames + mask1Frames + stimulusFrames + mask2Frames:
            mask2.draw()
            # if frameN == fixationFrames + mask1Frames + stimulusFrames:
            # print(core.getTime() - onset)
            # if frameN == cueFrames + fixationFrames + mask1Frames + stimulusFrames - 1:
            # win.getMovieFrame(buffer='front')
            # win.saveMovieFrames('%s samples.png' % stimulus['section'])
        win.flip()
    win.recordFrameIntervals = False

    # examine frame data and report any possible bad timings
    if checkStimulusTimings:
        intervals = np.array(win.frameIntervals)
        intervals = intervals[(ISIFrames + fixationFrames + mask1Frames - 1):(ISIFrames + fixationFrames + mask1Frames + stimulusFrames - 1)]
        win.frameIntervals = list([])
        nRequested = stimulusFrames
        nDisplayed = len(intervals) # an "interval" is the time between two flip() commands. So for n requested flips, PsychoPy records n - 1 intervals. confirmed in a minimal test case
        nBad = sum(intervals > 1 / monitorHz * 1.5)
        return({
            'frames.requested': nRequested,
            'frames.displayed': nDisplayed,
            'frames.dropped': nBad,
            'frames.stimTime': stimTime,
            'frames.stimTimeOffset': stimTime - stimulusFrames * (1 / monitorHz)
        })
    else:
        return({})


def displayStimulus(stimulus, settings, checkStimulusTimings = False, forceDuration = None):
    win = stimulus['window']
    monitorHz = float(settings['monitorHz'])

    ISIFrames = int(round(float(monitorHz) * 0.400))
    fixationFrames = int(round(0.5 * float(monitorHz)))
    mask1Frames = int(round(float(stimulus['maskInDuration']) * monitorHz))
    stimulusFrames = int(round(stimulus['presentationFrames'])) if forceDuration is None else forceDuration
    mask2Frames = int(round(float(stimulus['maskOutDuration']) * monitorHz))
    totalFrames = ISIFrames + fixationFrames + mask1Frames + stimulusFrames + mask2Frames

    if (stimulus['uppercase']):
        stimulus['text'] = stimulus['text'].upper()

    rectWidth = settings['rectWidth']
    rectHeight = settings['rectHeight']
    rectPos = (settings['rectX'], settings['rectY'])

    fixation_rectangle = visual.Rect(win, lineWidth=2, lineColor=stimulus['foreground'], width = rectWidth, height = rectHeight, units = 'pix', pos = rectPos)

    background_im = stimulus['background_image']
    background_video = stimulus['video_object']

    # masks
    mask1 = visual.TextStim(win,
                            text=makeMaskString(size=len(stimulus['text']) + 2),
                            font=stimulus['font'],
                            bold=stimulus['bold'] == 'TRUE',
                            italic=stimulus['italic'] == 'TRUE',
                            color=stimulus['foreground'],
                            height=stimulus['size'] * 1,
                            pos = rectPos
                            )
    mask2 = copy.copy(mask1)
    mask2.text = makeMaskString(size=len(stimulus['text']) + 2)

    # stimulus text
    stimulusText = copy.copy(mask1)
    stimulusText.text = stimulus['text']
    stimulusText.height = stimulus['size']

    if stimulus['textPosition'] == 'random':
        thePosition = (random.uniform(-rectWidth / 2 - rectPos[0] + 20, rectWidth / 2 - rectPos[0] - mask1.boundingBox[0] - 20),
                       random.uniform(-rectHeight / 2 - rectPos[1] + 20, rectHeight / 2 - rectPos[1] - mask2.boundingBox[1] - 20)
                       )
    elif stimulus['textPosition'] == 'center':
        thePosition = rectPos
        mask1.alignText = 'center'
        mask2.alignText = 'center'
        stimulusText.alignText = 'center'

    mask1.pos = thePosition
    mask2.pos = thePosition
    stimulusText.pos = thePosition

    # This loop counts frames and displays the stimulus.
    # Frame timing statistics SPECIFIC TO THE STIMULUS INTERVAL ONLY (no masks, fixation, etc) are captured if checkStimulusTimings = True

    if checkStimulusTimings:
        clock = core.Clock()
        stimTime = []
        win.recordFrameIntervals = True

    for frameN in range(0, totalFrames):
        background_im.draw()
        if background_video is not None:
            background_video.draw()
        if frameN < ISIFrames:
            pass
        elif frameN < ISIFrames + fixationFrames:
            if background_video is not None:
                fixation_rectangle.draw()
        elif frameN < ISIFrames + fixationFrames + mask1Frames:
            mask1.draw()
        elif frameN < ISIFrames + fixationFrames + mask1Frames + stimulusFrames:
            if checkStimulusTimings and frameN == ISIFrames + fixationFrames + mask1Frames:
                clock.reset()
            stimulusText.draw()
        elif frameN < ISIFrames + fixationFrames + mask1Frames + stimulusFrames + mask2Frames:
            if checkStimulusTimings and frameN == ISIFrames + fixationFrames + mask1Frames + stimulusFrames:
                stimTime = clock.getTime()
            mask2.draw()
        win.flip()

    if checkStimulusTimings:
        win.recordFrameIntervals = False

    # examine frame data and report any possible bad timings
    if checkStimulusTimings:
        intervals = np.array(win.frameIntervals)
        intervals = intervals[(ISIFrames + fixationFrames + mask1Frames - 1):(ISIFrames + fixationFrames + mask1Frames + stimulusFrames - 1)]
        win.frameIntervals = list([])
        nRequested = stimulusFrames
        nDisplayed = len(intervals) # an "interval" is the time between two flip() commands. So for n requested flips, PsychoPy records n - 1 intervals. confirmed in a minimal test case
        nBad = sum(intervals > 1 / monitorHz * 1.5)
        return({
            'frames.requested': nRequested,
            'frames.displayed': nDisplayed,
            'frames.dropped': nBad,
            'frames.stimTime': stimTime,
            'frames.stimTimeOffset': stimTime - stimulusFrames * (1 / monitorHz)
        })
    else:
        return({})


def getResponse(stimulus, messages, settings, subjectInfo, autoRT = None):
    win = stimulus['window']
    monitorHz = float(settings['monitorHz'])
    rectPos = (settings['rectX'], settings['rectY'])
    ISIFrames = round(monitorHz * 0.400)
    responseFrames = round(5 * monitorHz) if not autoRT else int(round(autoRT * monitorHz))
    maxFrames = int(ISIFrames + responseFrames)
    promptFont = settings['promptFont']
    promptSize = int(settings['promptSize'])
    giveFeedback = stimulus['giveFeedback']

    prompt = visual.TextStim(stimulus['window'],
                             text=messages['prompt'],
                             pos = rectPos,
                             font=promptFont,
                             color=stimulus['foreground'],
                             height=promptSize
                            )

    feedbackText = visual.TextStim(stimulus['window'],
                                   text=None,
                                   font=promptFont,
                                   color=stimulus['foreground'],
                                   height=promptSize,
                                   pos=[rectPos[0], rectPos[1] - promptSize - promptSize * 0.5]
                                )

    background_im = stimulus['background_image']

    f = 0
    response = []
    while f < maxFrames and not response:
        if stimulus['video_object'] is not None:
            stimulus['video_object'].draw()
        else:
            background_im.draw()

        if f >= ISIFrames:
            if f == ISIFrames:
                onset = core.Clock()
                event.clearEvents() # prevents keys from being stored from before prompt onset
            prompt.draw()
            response = event.getKeys(['1', '3', 'escape', 'num_1', 'num_3'], timeStamped = onset)

        win.flip()
        f += 1

    # process response
    if autoRT:
        return {'key': 'autopilot', 'rt': 'autopilot', 'correct': True}

    if not response:
        dataOut = {'key': 'NA', 'rt': 'NA', 'correct': 'NA'}
    elif response[0][0] == 'escape':
        dataOut = {'key': 'escape', 'rt': 'NA', 'correct': 'NA'}
        if stimulus['video_object']:
            stimulus['video_object'].pause()
        pauseScreen(stimulus['window'], messages, settings, subjectInfo)
        return dataOut
    else:
        responseIsWord = response[0][0] in ['1', 'num_1']
        responseIsCorrect = responseIsWord == stimulus['isWord']
        dataOut = {'key': response[0][0], 'rt': response[0][1], 'correct': responseIsCorrect}

    # calculate post-response frames
    feedbackFrames = 0
    if dataOut['correct'] == 'NA':
        feedbackFrames = int(monitorHz * 5)
    elif giveFeedback:
        feedbackFrames = int(monitorHz * 1)
    waitFrames = int(monitorHz * 0.250)
    totalFrames = feedbackFrames + waitFrames

    # define feedback text (if any)
    if not response:
        feedbackText.text = messages['feedbackSlow']
        feedbackText.color = '#cc0000'
    elif giveFeedback:
        feedbackText.text = messages['feedbackCorrect'] if responseIsCorrect else messages['feedbackIncorrect']

    # draw feedback
    for f in range(totalFrames):
        if stimulus['video_object'] is not None:
            stimulus['video_object'].draw()
        else:
            background_im.draw()
        prompt.draw()
        if (not response or giveFeedback) and f < feedbackFrames:
            feedbackText.draw()

        win.flip()

    return dataOut

def exportData(filename, trials):
    import csv, codecs

    fieldnames = sorted(trials[0].keys())
    for i, f in enumerate(fieldnames):
        fieldnames[i] = f

    with open(filename, 'w', encoding = 'utf-8') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames, restval='NA')
        writer.writeheader()
        for t in trials:
            writer.writerow(t)

# VIDEO FUNCTIONS

def fullscreen_movie(win, movie):

    movie_asp = movie.size[0] / movie.size[1]
    multiplier = win.size[1] / movie.size[1] if movie_asp > 1 else win.size[0] / win.size[1]
    x = movie.size[0] * multiplier
    y = movie.size[1] * multiplier

    movie.size = [x, y]
    return movie

def loadVideos(trials, wins):

    import pandas as pd
    from psychopy import visual

    videos = None

    if 'video' in trials[0]:
        unique_videos = pd.DataFrame(trials)[['section', 'video']].drop_duplicates()
        videos = dict.fromkeys(unique_videos.section)

        for index, row in unique_videos.iterrows():
            movie = visual.MovieStim3(wins[0], row['video'])
            videos[row['section']] = fullscreen_movie(wins[0], movie)

    return videos
