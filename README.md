# README #

## Description

The Glance Legibility Testing kit is a toolkit developed at the [MIT AgeLab][agelab] for a series of experiments that measure text legibility at a glance. The user is presented with a series of six-character words and non-words, and legibility is assessed by measuring the minimum time needed to discriminate between a word or non-word at about 80% accuracy. The program is built on Python's PsychoPy3 library. It is designed to make it relatively easy to change the font, colors (text and background), font size, and presentation method with a simple configuration file. Other configuration files are included to control other aspects of the experiment and provide for easier localization. A utility program is provided for measuring and adjusting the relative size of on-screen fonts. More details about the experimental methodology implemented in this code can be found in:

Dobres, J., Chahine, N., Reimer, B., Gould, D., Mehler, B., & Coughlin, J. F. (2016). Utilising psychophysical techniques to investigate the effects of age, typeface design, size and display polarity on glance legibility. Ergonomics, 59(10), 1377–1391. http://doi.org/10.1080/00140139.2015.1137637

## Authorship & Use

This toolkit was original developed for use at the Massachusetts Institute of Technology AgeLab (lead author Jonathan Dobres). This software is provided under an open source BSD license (see below).

## Recommended Citation

Dobres, J., Chahine, N., & Reimer, B. (2017). Effects of ambient illumination, contrast polarity, and letter size on text legibility under glance-like reading. Applied Ergonomics, 60(C), 68–73.

## BSD license

Copyright 2022 Massachusetts Institute of Technology

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

[agelab]: https://agelab.mit.edu