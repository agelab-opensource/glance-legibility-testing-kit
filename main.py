# imports. the first 2 lines are necessary to kill a pyglet bug that prevents the subject info dropdowns from working
import pyglet, random, numpy, pandas as pd
# pyglet.options['shadow_window'] = False
from psychopy import gui, visual, sound, core, event, data, logging
import os, ld, time, datetime, glob, re, sys

'''
====================
Recommended Citation
====================
Dobres, J., Chahine, N., & Reimer, B. (2017). Effects of ambient illumination,
contrast polarity, and letter size on text legibility under glance-like reading.
Applied Ergonomics, 60(C), 68–73.

============================
Released Under a BSD License
============================
Copyright 2022 Massachusetts Institute of Technology

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'''

random.seed(random.SystemRandom())

# load text for on-screen messages and prompts, as well as general experiment settings
messages = ld.importMessages('config/messages.csv')
settings = ld.importSettings('config/settings.csv')

# collect participant information
subjectInfo = ld.startupGUI(messages, settings)

# check for existing info, present options if found
dataCheck = ld.checkForPreviousData(messages, settings, subjectInfo)

# if resuming a session, set counters and data accordingly
if dataCheck['isResuming']:
    previousData = ld.loadPreviousData(dataCheck['file'], messages, settings)
    trials = previousData['trials']
    t = previousData['t']
    blockCounter = previousData['blockCounter']
    stair = previousData['stair']
else:
    # generates a list of dictionaries, one dict per trial of the experiment, containing all necessary attributes for the trial
    wordFile = settings['sess-%s-word' % subjectInfo[messages['guiSession']]]
    nonWordFile = settings['sess-%s-nonword' % subjectInfo[messages['guiSession']]]
    distractorFile = None if 'distractorFile' not in settings else settings['distractorFile']
    trials = ld.makeConditions('config/conditions.csv', wordFile, nonWordFile, distractorFile, float(settings['monitorHz']), subjectInfo)
    t = 0

# create uniquely timestamped filename for this participant
startTime = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H-%M-%S')
saveFile = 'Data/%s participant %s session %s %s.csv' % (settings['name'], subjectInfo[messages['guiID']], subjectInfo[messages['guiSession']], startTime)

breaks = ld.makeBreaks(trials, int(settings['blockLength']))
nBlocks = len(breaks) + 1

# save data file at start
ld.exportData(saveFile, trials)

# log file
#sys.stdout = open('Data/%s log.txt' % subjectInfo[messages['guiID']], 'w')

# open the windows
wins = list()
for w in range(settings['nScreens']):
    wins.append(
        visual.Window([1000, 1000],
                      allowGUI=False,
                      winType='pyglet',
                      units ='pix',
                      screen=w,
                      fullscr=True,
                      waitBlanking=True,
                      color = '#000000')
    )

# a little preamble to let everyone know that stuff if percolating....
loading_text = visual.TextStim(wins[0], text = 'Loading the experiment.\nThis may take a moment.', height = 50, font = settings['promptFont'])
loading_text.draw()
wins[0].flip()

# create any video objects
videos = ld.loadVideos(trials, wins)

# make all backgrounds
background_images = ld.make_all_backgrounds(wins[0], 'config/conditions.csv', settings)

# intro screen
ld.introScreen(wins[0], messages['intro'], settings)

# instruction screen
ld.instructionScreen(wins[0], messages, settings, trials[t]['section'])

practiceCorrect = 0
calibrated_thresholds = []
while t < len(trials):

    # trial timestamp
    trialStamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H-%M-%S')

    # reset blockCounter when we get to a new block
    blockCounter = 0 if t == 0 or trials[t]['section'] != trials[t - 1]['section'] else blockCounter

    # restart the staircase
    if blockCounter == 0:
        stair = ld.setStair(float(settings['monitorHz']))
    # update the staircase after the 9th trial
    elif blockCounter > 8 and trials[t - 1]['correct'] != 'NA':
        stair.addResponse(trials[t - 1]['correct'])
        stair.calculateNextIntensity()
        stair.next()

    # if this trial does not have a predefined presentation time, use the staircase value
    # also corrects for an edge case in which the 9th trial of the block does not have a valid response
    if trials[t]['presentationFrames'] == 'NA':
        trials[t]['presentationFrames'] = 0.2 * float(settings['monitorHz']) if len(stair.intensities) == 0 else stair.intensities[len(stair.intensities) - 1]
    elif trials[t]['presentationFrames'] == 'calibrated':
        trials[t]['presentationFrames'] = round(numpy.mean(calibrated_thresholds))

    stimulus = dict(trials[t])
    for w in range(len(wins)):
        if trials[t]['screen'] == w:
            stimulus['window'] = wins[w]

    # grab background image
    stimulus['background_image'] = background_images[stimulus['section']]

    # grab video
    stimulus['video_object'] = None if videos is None else videos[stimulus['section']]

    if stimulus['video_object'] is not None:
        stimulus['video_object'].play()

    # display stimulus and collect response1
    if trials[t]['nRows'] > 1 or trials[t]['nCols'] > 1:
        timings = ld.displayCrowdingStimulus(stimulus, settings, messages)
    else:
        timings = ld.displayStimulus(stimulus, settings, checkStimulusTimings = False)
    response = ld.getResponse(stimulus, messages, settings, subjectInfo, autoRT = False)

    # update trial data and save
    trials[t].update(response)
    trials[t].update({'soa': float(stimulus['presentationFrames']) / float(settings['monitorHz']) if type(stimulus['presentationFrames']) is not str else 'open response', 'completed': True, 'timestamp': trialStamp})
    if trials[t]['duration'] != 'rt':
        trials[t].update(timings)
    ld.exportData(saveFile, trials)

    # if this is the end of a staircase block, calculate a threshold and store it in calibrated_threshold array
    if t != len(trials) - 1 and trials[t]['section'] != trials[t + 1]['section'] and trials[t]['duration'] == 'stair':
        matching_trials = [a['presentationFrames'] for a in trials if a['section'] == trials[t]['section']]
        calibrated_thresholds.append(numpy.median(matching_trials[-20:]))

    # keep track of consecutive correct responses during practice block
    practiceCorrect = practiceCorrect + 1 if stimulus['section'] == 'practice' and response['correct'] == True else 0

    # handling for rest periods, practice loop and end screen
    if t == len(trials) - 1:
        ld.endScreen(wins[trials[t]['screen']], messages, settings, trials)
    elif t in breaks:
        sameConditionNext = trials[t]['section'] == trials[t + 1]['section']
        sameGroupNext = trials[t]['group'] == trials[t + 1]['group']

        breakMessage = ''
        breakTimer = 30
        breakOptional = True
        if sameConditionNext:
            breakMessage = messages[settings['breakMessageWithinCondition']]
            breakTimer = settings['breakLengthWithinCondition']
            breakOptional = settings['breakOptionalWithinCondition']
        elif not sameConditionNext and sameGroupNext:
            breakMessage = messages[settings['breakMessageBetweenCondition']]
            breakTimer = settings['breakLengthBetweenCondition']
            breakOptional = settings['breakOptionalBetweenCondition']
        elif not sameGroupNext:
            breakMessage = messages[settings['breakMessageBetweenGroup']] % trials[t + 1]['section']
            breakTimer = settings['breakLengthBetweenGroup']
            breakOptional = settings['breakOptionalBetweenGroup']

        # Must pause videos before the break screen.
        # PsychoPy is tracking time for any video in a "play" state. After the break, video will play very rapidly to "catch up" to the correct time index.
        if stimulus['video_object'] is not None:
            stimulus['video_object'].pause()

        ld.breakScreen(
            wins[trials[t + 1]['screen']],
            messages['breakIntro'],
            breakMessage,
            settings,
            breakTimer,
            breakOptional,
            breaks.index(t) + 1,
            nBlocks
        )
    elif (trials[t]['section'] == 'practice' and t == 9) or practiceCorrect >= 5:
        willPracticeAgain = ld.practiceAgain(wins[trials[10]['screen']], messages, settings, trials[10]['distance'])
        if willPracticeAgain:
            t = -1
            practiceCorrect = 0
            for i in range(10):
                trials[i]['completed'] = False
        else:
            t = 9
            for i in range(10):
                trials[i]['completed'] = True

    t = t + 1
    blockCounter = blockCounter + 1

# save complete data file at end
ld.exportData(saveFile, trials)

# save stimulus timings

for i in range(settings['nScreens']):
    timingFile = 'Data/timings %s participant %s session %s win %i.txt' % (settings['name'], subjectInfo[messages['guiID']], subjectInfo[messages['guiSession']], i)
    wins[i].saveFrameIntervals(fileName=timingFile, clear=True)

for w in wins:
    w.close()
#sys.stdout.close()
core.quit()
